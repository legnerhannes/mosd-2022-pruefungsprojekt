# Dokumentation

Das Ziel dieses Projektes ist es zu Erfahren, wie verschiedene ausgewählte Staaten Europas durch Corona betroffen waren. Als Basis hierfür werden die Fallzahlen aus dem Covid-19 Datensatz des Johns Hopkins University Center for Systems Science and Engineering genommen: https://github.com/datasets/covid-19

Weiterhin wurden zur Errechnung der 7-Tage-Inzidenz Schätzungen für Einwohnerzahlen von ec.europa.eu/eurostat verwendet:
https://ec.europa.eu/eurostat/documents/2995521/11081097/3-10072020-AP-DE.pdf/7f863daa-c1ac-758f-e82b-954726c4621f#:~:text=Am%201.,Januar%202019.´

Metadaten zu dem Projekt lassen sich in *metadata.xml* finden und wurden generiert durch den Dublin Core Generator: 
https://nsteffel.github.io/dublin_core_generator/generator_nq.html

## Skripte

Es wurden mehrere Python Skripte geschrieben, um eine teilweise Automatisierung  bei der Erstellung, Analyse und Qualitätskontrolle der Daten zu ermöglichen.

### GetDailyCases.py

Dieser Skript nimmt die Datei *countries-aggregated.csv* als Input und filtert die nötigen Informationen heraus. Dabei handelt es sich um die Fallzaheln für die Staaten Deutschland, Frankreich, Italien, Niederlande, Spanien, Vereinigtes Königreich und Schweden, welche zusätzlich von der aufsummierten Gesamtzahl der Fälle zu den täglichen Erhöhungen transformiert werden. Diese Daten werden in der Datei *daily-cases.csv* gespeichert.

### GetIncidence.py

Hier wird durch die Fallzahlen aus *daily-cases.csv* und den Einwohnerzahlen der einzelnen Staaten für 2020, welche aus *inhabitants-2020.csv* genommen werden, die 7-Tage-Inzidenz über den gegebenen Zeitraum ausgerechnet. Dies wird für die ausgewählten 7 Staaten getan und anschließend in der Datei *daily-incidence.csv* gespeichert. Die Formel zu Errechnung der 7-Tage-Inzidenz lautet *(Anzahl der Fälle der letzen 7 Tage / Einwohnerzahl) * 100000*.

### QualityControl.py

Durch dieses Skript wird die Qualität der genutzen *.csv* Dateien auf Teile der *Data Quality Dimensions* getestet. Folgende Aspekte werden Einbezogen:

- Completeness: Überprüfung, ob alle Tage im gegebenen Zeitraum abgedeckt wurden.
- Uniqueness: Überprüfung, ob jeder Eintrag einzigartig ist. (Jeder Staat darf nur einen Eintrag für jedes Datum haben)
- Timeliness: Überprüfung, ob die Daten in richtiger chronologischer Reihenfolge sind.

### VisualizeData.py

Durch diesen Skript werden die Graphen erstellt, welche in diesen Projekt zu finden sind. Dazu nutzt es die Bibliothek *matplotlib.pyplot* und liest die Daten aus der Datei *daily-incidence.csv*. Die x-Achse stellt hierbei die Zeitlinie dar, während die y-Achse auf verschiedene Attribute wie Fallzahlen oder Inzidenz gemappt wurde. Die verschiedenen Länder werden als unterschiedlich Farbige Liniengraphen dargestellt.


## Daten

Die Daten werden in Form von *.csv* Dateien gespeichert. Dies wurde so gewählt, da das Covid-19 Projekt der Johns Hopkins Universität diese bereits nutzt, es sich um ein standardisiertes Dateiformat handelt und sich das Tabellenformat gut für die hier bearbeiteten Daten anbietet. Die folgenden Tabellen dienen zu genaueren Erklärung für die Spalten der verscheidenen *.csv* Dateien.

### countries-aggregated.csv

Feldname | Datentyp | Inhalt
-------- | -------- | --------
Date   | date   | Datum der Daten
Country   | string   | Name des Staates
Confirmed   | int   | Summe aller bestätigten Fälle
Recovered   | int   | Summe aller genesene Fälle
Deaths   | int   | Summe aller Tode durch Covid-19


### daily-cases.csv

Feldname | Datentyp | Inhalt
-------- | -------- | --------
Date   | date   | Datum der Daten
Country   | string   | Name des Staates
Confirmed   | int   | Tägliche bestätigte Fälle
Recovered   | int   | Tägliche genesene Fälle
Deaths   | int   | Tägliche Tode durch Covid-19


### inhabitants-2020.csv

Feldname | Datentyp | Inhalt
-------- | -------- | --------
Country   | string   | Name des Staates
Inhabitants   | int   | Geschätzte Bevölkerungszahl

### daily-incidence.csv

Feldname | Datentyp | Inhalt
-------- | -------- | --------
Date   | date   | Datum der Daten
Country   | string   | Name des Staates
Incidence  | float   | 7-Tage-Inzidenz