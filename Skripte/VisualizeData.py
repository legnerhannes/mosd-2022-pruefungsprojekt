"""
	@author: 	Maximilian Raupach
	@Date:		2021.07.29
"""
import csv
from datetime import datetime
import matplotlib.pyplot as plt

path_inzidenz = "../Daten/countries-aggregated.csv"
path_countries = "../Daten/inhabitants-2020.csv"

"""
	Name:			getCountryName
	Parameter:		none
	returnvaule:	List of Country Names
"""
def getCountryName():
	list = []
	f = open(path_countries, "r")
	reader = csv.reader(f)
	
	# skipping the first line in the csv file
	next(reader)
	
	for row in reader:
		list.append(row[0])
	
	f.close()
	return list
	
"""
	name:			getRowsFromCountry
	parameter:		country - name of Country you want the rows
	returnvaule:	List of CSV rows of of Country
"""
def getRowsFromCountry(country):
	f = open(path_inzidenz, "r")
	
	reader = csv.reader(f)
	
	rows = []
	for row in reader:
		if(row[1] == country):
			rows.append(row)
	
	f.close()
	return rows
	
"""
	name:			getDatetimesFromRows
	parameter:		rows - list of Rows you want the Datetimes
	returnvaule:	List of Datetimes
"""
def getDatetimesFromRows(rows):
	list = []
	
	for row in rows:
		list.append(datetime.fromisoformat(row[0]))
		
	return list
	
"""
	name:			getInzidenzFromsRows
	parameter:		rows - list of Rows you want the InzidenzValues
	returnvaule:	List of InzidenzValues
"""	
def getInzidenzFromsRows(rows):
	list = []
	
	for row in rows:
		list.append(float(row[2]))
	
	return list

	


"""
	Programm that visulaizes all Countries
"""
countries = getCountryName()
for country in countries:
	rows = getRowsFromCountry(country)
	dates = getDatetimesFromRows(rows)
	inzidenz = getInzidenzFromsRows(rows)

	plt.plot(dates, inzidenz, label=country)
	
plt.legend(loc="upper left")
plt.show()