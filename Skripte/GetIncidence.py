"""
	@author: Maximilian Raupach
	@Date: 2021.07.26
"""
import csv

path_inhabitants = "../Daten/inhabitants-2020.csv"
path_increaseRate = "../Daten/daily-cases.csv"
path_inzidenzFile = "../Daten/daily-incidence.csv"

def getCovidRowsByCountry(country):
	countryRows = []
	with open(path_increaseRate, newline='') as f:
		reader = csv.reader(f)
		for row in reader:
			if(row[1] == country):
				countryRows.append(list(row))
	return countryRows

def getInhabitantsByCountryAndYear(country):
	with open(path_inhabitants, newline='') as f:
		reader = csv.reader(f)
		for row in reader:
			if(row[0] == country):
				return float(row[1])
	return None
		
def sumUpConfirmedRowValues(rows, StartValue=0, EndValue=6):
	confirmed = 0
	
	if(StartValue < EndValue and StartValue < len(rows) and EndValue < len(rows)):
		for i in range(StartValue, EndValue+1):
			confirmed += int(rows[i][2])

	return confirmed
		
def createInzidenzForCountry(country):
	inhib =  getInhabitantsByCountryAndYear(country)
	
	rows = getCovidRowsByCountry(country)
	
	f = open(path_inzidenzFile, "a")
	
	for i in range(6, len(rows)):
		incidence = sumUpConfirmedRowValues(rows, i-6, i) / inhib * 100000
		
		f.write(f"{rows[i][0]},{rows[i][1]},{round(incidence,2)}\n")
			
	f.close()
	
def writeHeader():
	f = open(path_inzidenzFile, "w")
	
	f.write("Date,Country,Incidence\n")
	
	f.close()
	
	
writeHeader()
createInzidenzForCountry("Germany")
createInzidenzForCountry("France") 
createInzidenzForCountry("Italy")
createInzidenzForCountry("Netherlands")
createInzidenzForCountry("Spain")
createInzidenzForCountry("United Kingdom")
createInzidenzForCountry("Sweden")