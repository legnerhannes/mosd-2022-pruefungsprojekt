"""
	@author: Maximilian Raupach
	@Date: 2021.07.26
"""
import csv

path_covid_19 = "../Daten/countries-aggregated.csv"
path_increaseRate ="../Daten/daily-cases.csv"
	
def getCovidRowsByCountry(country):
	countryRows = []
	with open(path_covid_19, newline='') as f:
		reader = csv.reader(f)
		for row in reader:
			if(row[1] == country):
				countryRows.append(list(row))
	return countryRows
	
def calcIncreseRate(country):
	rows = getCovidRowsByCountry(country)
	Confirmed = 0
	Recovered = 0
	Deaths = 0
	
	f = open(path_increaseRate, "a")
	
	for row in rows:
		c = int(row[2]) - Confirmed
		r = int(row[3]) - Recovered
		d = int(row[4]) - Deaths
		
		# hier wird gegen gesteuert (bei negtiven werten liegt hier ein Fehler vor --> gegen gesteuert mit Annahme = 0!
		if(c < 0):
			c = 0
		if(r < 0):
			r = 0
		if(d < 0):
			d = 0
		f.write(f"{row[0]},{row[1]},{c},{r},{d}\n")
		
		if(not c == 0):
			Confirmed = int(row[2]) 
		if(not r == 0):
			Recovered = int(row[3])
		if(not d == 0):
			Deaths = int(row[4])
		
	f.close()
		
def writeHeader():
	f = open(path_increaseRate, "w")
	
	f.write("Date,Country,Confirmed,Recovered,Deaths\n")
	
	f.close()
	
	
writeHeader()	
calcIncreseRate("Germany")
calcIncreseRate("France") 
calcIncreseRate("Italy")
calcIncreseRate("Netherlands")
calcIncreseRate("Spain")
calcIncreseRate("United Kingdom")
calcIncreseRate("Sweden")