import datetime
import csv


inhibitantsPath = "../Daten/inhabitants-2020.csv"
csvToCheck = "../Daten/countries-aggregated.csv"



def getRowsForCountry(country):
	list = []
	path = csvToCheck
	
	f = open(path, "r")
	reader = csv.reader(f)
	for row in reader:
		if(row[1] == country):
			list.append(row)
			
	f.close()
	
	return list


def getCountryName():
	list = []
	path = inhibitantsPath
	f = open(path, "r")
	reader = csv.reader(f)
	
	next(reader)

	for row in reader:
		list.append(row[0])
	
	f.close()
	return list



def countDatesForCountry(country):
	path = csvToCheck

	f = open(path, "r")
	reader = csv.reader(f)
	count = 0
	for row in reader:
		if(row[1] == country):
			count += 1
			
	f.close()
	
	return count


def getMinMaxDate():
	rows = getRowsForCountry("United Kingdom")
	start = datetime.datetime.strptime(rows[0][0], "%Y-%m-%d")
	end = datetime.datetime.strptime(rows[0][0], "%Y-%m-%d")

	for row in rows:
		date = datetime.datetime.strptime(row[0], "%Y-%m-%d")
		if(date < start):
			start = date
		elif(date > end):
			end = date
			
	return (start, end)



#COMPLETNESS
#kontrolliert ob alle Tage abgedeckt sind --> anpassen an länder die ich unter die Lupe nehmen will

names = getCountryName()
StartDate, EndDate = getMinMaxDate()

dayCount = (EndDate - StartDate).days + 1 

Germany 	= countDatesForCountry("Germany")
France 		= countDatesForCountry("France")
Italy 		= countDatesForCountry("Italy")
Netherlands = countDatesForCountry("Netherlands")
Spain 		= countDatesForCountry("Spain")
UK 			= countDatesForCountry("United Kingdom")
Sweden 		= countDatesForCountry("Sweden")

print(f"Germany: 		{Germany/dayCount*100}% ({Germany}/{dayCount}) completeness")
print(f"France: 		{France/dayCount*100}% ({France}/{dayCount}) completeness")
print(f"Italy: 			{Italy/dayCount*100}% ({Italy}/{dayCount}) completeness")
print(f"Netherlands: 		{Netherlands/dayCount*100}% ({Netherlands}/{dayCount}) completeness")
print(f"Spain: 			{Spain/dayCount*100}% ({Spain}/{dayCount}) completeness")
print(f"UK: 			{UK/dayCount*100}% ({UK}/{dayCount}) completeness")
print(f"Sweden: 		{Sweden/dayCount*100}% ({Sweden}/{dayCount}) completeness")
print(f"Aggregated:		{(Germany+France+Italy+Netherlands+Spain+UK+Sweden)/(7*dayCount)*100}% ({Germany+France+Italy+Netherlands+Spain+UK+Sweden}/{7*dayCount}) completeness")

print("\n==================================================\n")




def checkOrderDates(rows):
	date = datetime.datetime.strptime(rows[0][0], "%Y-%m-%d")
	for row in rows:
		curDate = datetime.datetime.strptime(row[0], "%Y-%m-%d")
		if(date > curDate):
			return False
		else:
			date = curDate
			
	return True

#checkt ob die tage  in Richtiger Reihenfolge angeordnet sind
countries = getCountryName()

for country in countries:
	rows = getRowsForCountry(country)
	print(f"{country} - {checkOrderDates(rows)}")

print("\n==================================================\n")





def calcUniquessforCountry(country):
	
	rows = getRowsForCountry(country)
	UniqElements = []
	for row in rows:
		if(not (row[0] in UniqElements)):
			UniqElements.append(row[0])
			
	calcUniquessforCountry.UniqCount += len(UniqElements)
	return dayCount/len(UniqElements)*100


#Überprüft, ob Einträge einzigartig sind
calcUniquessforCountry.UniqCount = 0	
countries 	= getCountryName()
for country in countries:
	uniq = calcUniquessforCountry(country)
	print(f"{country} - {uniq}% Uniqueness")
	
print("__________________________________")
print(f"Aggregated	{dayCount*len(countries)/calcUniquessforCountry.UniqCount*100}%	Uniqueness")

print("\n==================================================\n")



def getInvalidRows(rows):
	lastValue = 0
	invaliedRows = []
	for row in rows:
		if(lastValue > int(row[2])):
			invaliedRows.append(row[0])

		lastValue = int(row[2])

	return invaliedRows
	
	
invaliedCount = 0
rowCount = 0
countries = getCountryName()
for country in countries:
	rows = getRowsForCountry(country)
	rowCount += len(rows)
	inValiedRows = getInvalidRows(rows)
	invaliedCount += len(inValiedRows)
	print(f"{country}		{round((len(rows) - len(inValiedRows))/len(rows)*100,2)}%	Validity")
	if(len(inValiedRows) > 0):
		print("invalied Dates:")
		print(",".join(inValiedRows))
	print(f"___________________")
	
print(f"aggreggated	{round((rowCount-invaliedCount)/rowCount*100,2)}%	Validity")